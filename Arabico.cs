namespace NumRomanos
{
    public class Arabico
    {
        public int Convertidor(string txtRomano)
        {
            txtRomano = txtRomano.ToUpper();
            int Resultado = 0;
            
            foreach (char letter in txtRomano)
            {
                Resultado += letterTotxtRomano(letter);
            }

            if (txtRomano.Contains("IV")||txtRomano.Contains("IX"))
            {
                Resultado -= 2;
            }
            if (txtRomano.Contains("XL")||txtRomano.Contains("XC"))
            {
                Resultado -= 20;
            }
            if (txtRomano.Contains("CD")||txtRomano.Contains("CM"))
            {
                Resultado -= 200;
            }
            return Resultado;
        }
        public int letterTotxtRomano(char letter)
        {
            switch (letter)
            {
                case 'M':
                {
                    return 1000;
                }
                case 'D':
                {
                    return 500;
                }
                case 'C':
                {
                    return 100;
                }
                case 'L':
                {
                    return 50;
                }case 'X':
                {
                    return 10;
                }
                case 'V':
                {
                    return 5;
                }
                case 'I':
                {
                    return 1;
                }
                default:
                {
                    return 0;
                }
            }
        }
    }
}
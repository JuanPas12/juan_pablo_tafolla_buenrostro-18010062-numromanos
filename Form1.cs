using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NumRomanos.Properties;
namespace NumRomanos
{
    public partial class Form1 : Form
    {
        Roman C2;
        Arabico C1;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }
        private void txtArabigo_TextChanged(object sender, EventArgs e)
        {
            throw new System.NotImplementedException();
        }
        private void Button_a_Romano_Click(object sender, EventArgs e)
        {
            C2=new Roman();
            int Arab = int.Parse(txtArabigo.Text.ToString());
            string ResRom = C2.ConvertidorRomano(Arab);
            lblRomano.Text ="Resultado: "+ResRom;
        }
        private void Button_a_Arabigo_Click(object sender, EventArgs e)
        {
            C1=new Arabico();
            string NumeroRomano = txtRomano.Text.ToString();
            int Arabico = C1.Convertidor(NumeroRomano);
            lblArabigo.Text ="Resultado: "+ Arabico;
        }
    }
}
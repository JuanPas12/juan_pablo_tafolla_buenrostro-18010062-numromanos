using System.Windows.Forms;

namespace NumRomanos
{
    public class Roman
    {
        public string ConvertidorRomano(int numero)
        {
            string Rom="";
            while (true)
            {
                if (numero<4000&&numero>999)
                {
                    switch (numero/1000)
                    {
                        case 1:
                            Rom += "M";
                            break;
                        case 2:
                            Rom += "MM";
                            break;
                        case 3:
                            Rom += "MMM";
                            break;
                    }
                    numero -= 1000*(numero/1000);
                }
                else if (numero<1000&&numero>99)
                {
                    switch (numero/100)
                    {
                        case 1: Rom += "C"; break;
                        case 2: Rom += "CC"; break;
                        case 3: Rom += "CCC"; break;
                        case 4: Rom += "CD"; break;
                        case 5: Rom += "D"; break;
                        case 6: Rom += "DC"; break;
                        case 7: Rom += "DCC"; break;
                        case 8: Rom += "DCCC"; break;
                        case 9: Rom += "CM"; break;
                    }
                    numero -= 100*(numero/100);
                }
                else if (numero<100&&numero>10)
                {
                    switch (numero/10)
                    {
                        case 1: Rom += "X"; break;
                        case 2: Rom += "XX"; break;
                        case 3: Rom += "XXX"; break;
                        case 4: Rom += "XL"; break;
                        case 5: Rom += "L"; break;
                        case 6: Rom += "LX"; break;
                        case 7: Rom += "LXX"; break;
                        case 8: Rom += "LXXX"; break;
                        case 9: Rom += "XC"; break;
                    }
                    numero -= 10*(numero/10);
                }
                
                else if (numero<9&&numero>0)
                {
                    switch (numero)
                    {
                        case 1: Rom += "I"; break;
                        case 2: Rom += "II"; break;
                        case 3: Rom += "III"; break;
                        case 4: Rom += "IV"; break;
                        case 5: Rom += "V"; break;
                        case 6: Rom += "VI"; break;
                        case 7: Rom += "VII"; break;
                        case 8: Rom += "VIII"; break;
                        case 9: Rom += "IX"; break;
                    }
                    numero -=numero;
                }
                else
                {
                    MessageBox.Show("El numero esta fuera del rango");
                    break;
                }
                if (numero==0)
                {
                    break;
                }
            }
            return Rom;
        }
    }
}